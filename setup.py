from setuptools import setup

setup(
    name='GeniusToolKit',
    version='1.6',
    packages=['gtk',],
    url='',
    license='',
    author='Genius',
    author_email='enanje@gmail.com',
    description='A toolkit to make Pythonic life easier.'
)
