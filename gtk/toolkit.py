# This will be an all in one Python toolkit that will make programming with Python Easier!
import logging
import os
import sys
import inspect

# from colorama import Fore, Back, Style

print_logs = True

if not os.path.exists("logs"):
    os.mkdir("logs")
logging.basicConfig(filename='logs/logs.txt',
                    filemode='a',
                    format='\n%(asctime)s,%(msecs)d %(module)s %(levelname)s: %(message)s',
                    datefmt='%B %d %H:%M:%S %p',
                    level=logging.INFO)


class Settings:
    """Static methods"""

    @staticmethod
    def toggle_logging():
        global print_logs
        print_logs = True if not print_logs else False
        print(f"Print logging is now {'On' if print_logs else 'Off'}")


def printl(*args: list, color='BLUE', bg="RESET"):
    """Will print msg if print_logs=True"""
    string = ""
    for a in args:
        if isinstance(a, list):
            string += "############# Start of Array #############\n"
            for i, each in enumerate(a):
                string += f"{str(each)}\n"
            string += "############# End of Array #############\n"
        else:
            string += f"{str(a)}\n"
    # color = color.upper()
    # bg = bg.upper()
    if not print_logs:
        return
    print(f"{string}")
    # eval(f"Fore.{color}") + eval(f"Back.{bg}") +


def log(msg: str, *args, **kwargs):
    current_frame = inspect.currentframe()
    call_frame = inspect.getouterframes(current_frame, 2)[1]
    caller = call_frame[4][0].strip("")
    if caller == "\n":
        caller = call_frame[4][1]
    caller = caller.strip("\n").strip("def ").strip(":")
    filename = call_frame[1].split("/")[-1:][0]
    lineno = call_frame[2]
    string = "\n"
    segment = f"######### Log From {caller} in {filename}  line {lineno} #########"
    string += segment
    string += f'\n\nMessage: {msg}\nArguments: '

    for a in args:
        string += f"{a}, "
    for key, value in kwargs.items():
        string += f"{key}: {value}, "

    placeholder = "{:#^{len}}".format("", len=len(segment))
    endsegment = f"\n\n{placeholder}"
    string += endsegment
    logging.info(string)


def print_callframe():
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)[1]
    caller = calframe[4][0].strip("\n")
    print([i for i in calframe])
    print(caller)